function register(voxaApp) {
  voxaApp.onIntent("LaunchIntent", {
    flow: "continue",
    reply: "Welcome",
    to: "askHowManyWins",
  });

  voxaApp.onState("askHowManyWins", {
    flow: "yield",
    reply: "AskHowManyWins",
    to: "getHowManyWins",
  });

  voxaApp.onState("getHowManyWins", voxaEvent => {
    if (voxaEvent.intent.name !== "MaxWinsIntent") {
      return {
        to: voxaEvent.intent.name,
      };
    }
    const param = voxaEvent.intent.params.wins;
    voxaEvent.model.wins = param;
    voxaEvent.model.userWins = 0;
    voxaEvent.model.alexaWins = 0;

    if (param > 10) {
      return {
        flow: "yield",
        reply: "BigNumber",
        to: "askToContinueBigNumber",
      };
    }
    return {
      flow: "continue",
      reply: "StartGame",
      to: "askUserChoice",
    };
  });

  voxaApp.onState(
    "askToContinueBigNumber",
    {
      flow: "continue",
      to: "askHowManyWins",
    },
    "NoIntent"
  );
  voxaApp.onState(
    "askToContinueBigNumber",
    {
      flow: "continue",
      reply: "StartGame",
      to: "askUserChoice",
    },
    "YesIntent"
  );

  const CHOICES = ["rock", "paper", "scissors"];

  voxaApp.onState("askUserChoice", voxaEvent => {
    const { wins, userWins, alexaWins } = voxaEvent.model;
    const userWon = parseInt(userWins) >= parseInt(wins);
    const alexaWon = parseInt(alexaWins) >= parseInt(wins);
    let reply = userWon
      ? "UserWinsTheGame"
      : alexaWon
      ? "AlexaWinsTheGame"
      : "AskUserChoice";

    if (!userWon && !alexaWon) {
      const min = 0;
      const max = CHOICES.length - 1;
      voxaEvent.model.userChoice = undefined;
      voxaEvent.model.alexaChoice =
        Math.floor(Math.random() * (max - min + 1)) + min;

      return {
        flow: "yield",
        reply,
        to: "getUserChoice",
      };
    }

    return {
      flow: "continue",
      reply,
      to: "askIfStartANewGame",
    };
  });

  voxaApp.onState("getUserChoice", voxaEvent => {
    let { userChoice } = voxaEvent.model;
    if (voxaEvent.intent.name === "RockIntent") {
      userChoice = "rock";
    } else if (voxaEvent.intent.name === "PaperIntent") {
      userChoice = "paper";
    } else if (voxaEvent.intent.name === "ScissorsIntent") {
      userChoice = "scissors";
    }

    if (userChoice) {
      return {
        flow: "continue",
        to: "processWinner",
      };
    } else {
      return {
        to: voxaEvent.intent.name,
      };
    }
  });
  voxaApp.onState("processWinner", voxaEvent => {
    const { userChoice, alexaChoice } = voxaEvent.model;
    const alexaChose = CHOICES[alexaChoice];
    let isUserWinning = false;

    const transition = {
      flow: "continue",
      reply: "TiedResult",
      to: "askUserChoice",
    };

    if (alexaChose === userChoice) {
      return transition;
    }
    switch (alexaChose) {
      case "rock":
        isUserWinning = userChoice === "paper";
        break;
      case "paper":
        isUserWinning = userChoice === "scissors";
        break;
      case "scissors":
        isUserWinning = userChoice === "rock";
        break;
    }

    isUserWinning
      ? ((voxaEvent.model.userWins += 1), (transition.reply = "UserWins"))
      : ((voxaEvent.model.alexaWins += 1), (transition.reply = "AlexaWins"));

    return transition;
  });
  voxaApp.onState("askIfStartANewGame", {
    flow: "yield",
    reply: "AskIfStartANewGame",
    to: "shouldStartANewGame",
  });
  voxaApp.onState("shouldStartANewGame", voxaEvent => {
    if (voxaEvent.intent.name === "YesIntent") {
      return {
        flow: "continue",
        reply: "RestartGame",
        to: "askHowManyWins",
      };
    }
    return {
      to: "GoodBye",
    };
  });
  voxaApp.onIntent("CancelIntent", {
    to: "GoodBye",
  });

  voxaApp.onIntent("StopIntent", {
    to: "GoodBye",
  });

  voxaApp.onState("GoodBye", {
    flow: "terminate",
    reply: "Bye",
  });

  voxaApp.onIntent("GameScoreIntent", voxaEvent => {
    const { wins, userWins, alexaWins } = voxaEvent.model;

    if (!wins) {
      return {
        flow: "continue",
        reply: "NoWins",
        to: "askHowManyWins",
      };
    }
    if (!userWins && !alexaWins) {
      return {
        flow: "yield",
        reply: "NoScore",
        to: "handleNoScore",
      };
    }

    return {
      flow: "yield",
      reply: "Score",
      to: "resumeGame",
    };
  });

  voxaApp.onState("handleNoScore", voxaEvent => {
    if (voxaEvent.intent.name === "YesIntent") {
      return {
        flow: "continue",
        reply: "StartGame",
        to: "askUserChoice",
      };
    }

    return {
      to: "GoodBye",
    };
  });

  voxaApp.onState("resumeGame", voxaEvent => {
    if (voxaEvent.intent.name === "YesIntent") {
      return {
        flow: "continue",
        to: "askUserChoice",
      };
    }
    return {
      to: "GoodBye",
    };
  });
  voxaApp.onIntent("NewGameIntent", {
    flow: "yield",
    reply: "ConfirmRestartGame",
    to: "confirmGame",
  });

  voxaApp.onState("confirmGame", voxaEvent => {
    let state =
      voxaEvent.intent.name === "YesIntent"
        ? "askHowManyWins"
        : "askUserChoice";

    return {
      flow: "continue",
      to: state,
    };
  });

  voxaApp.onIntent("HelpIntent", {
    flow: "continue",
    reply: "Help",
  });

  voxaApp.onState("FallbackIntent", {
    flow: "continue",
    reply: "DidNotUnderstand",
  });
}

module.exports = register;
